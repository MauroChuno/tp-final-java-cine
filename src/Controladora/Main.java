/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import VistaRegistroPeli.VistaAdmRegistro;
import modelo.RepertorioPeliculas;

/**
 *
 * @author maria
 */
public class Main {
    public static void main(String[] args) {
        ControladoraCine control1 = new ControladoraCine();
        RepertorioPeliculas repertorio1 = new RepertorioPeliculas();
        VistaAdmRegistro PPrincipal = new VistaAdmRegistro ();
        
        repertorio1.setControladoraCine (control1);
        PPrincipal.setControladoraCine (control1);
        
        control1.setRepertorioPeliculas (repertorio1);
        control1.setVistaAdmRegistro (PPrincipal);
        
        PPrincipal.setVisible(true);
    }
}
