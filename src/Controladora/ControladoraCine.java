/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import VistaRegistroPeli.VistaAdmRegistro;
import javax.swing.JButton;
import javax.swing.JComboBox;
import modelo.Genero;
import modelo.PaisDeOrigen;
import modelo.Pelicula;
import modelo.RepertorioPeliculas;

/**
 *
 * @author alumno
 */
public class ControladoraCine {
    
    private RepertorioPeliculas repertorio1;
    private VistaAdmRegistro PPrincipal;
    
    //escribir métodos
    Pelicula peli = new Pelicula();
    
    /**
     *
     * @param txtNombre
     * @return 
     */
    public Pelicula registrarNombre (String nombre){
        peli.setNombre(nombre);
        return peli;
    }
    

    /**
     *
     * @param listPais
     * @return
     */
    public Pelicula registrarPais(javax.swing.JComboBox<String> listPais) {
        PaisDeOrigen pais = new PaisDeOrigen ();
        peli.setPaisOrigen(pais);
        return peli;
    }
    
    public Pelicula registrarGenero(javax.swing.JComboBox<String> listGenero) {
        Genero gen = new Genero ();
        peli.setGenero(gen);
        return peli;
    }
    
    void setRepertorioPeliculas(RepertorioPeliculas repertorio1) {
        this.repertorio1 = repertorio1;
    }

    void setVistaAdmRegistro(VistaAdmRegistro PPrincipal) {
        this.PPrincipal = PPrincipal;
    }

    public Pelicula registrarDuracion(javax.swing.JTextField txtDuracion) {
        String d = txtDuracion.getText();
        double duracion = Double.parseDouble(d);
        peli.setDuracion(duracion);
        return peli;
    }

    public Pelicula registrarTOriginal(javax.swing.JTextField txtTituloOriginal) {
        String titulo = txtTituloOriginal.getText();
        peli.setTituloOriginal(titulo);
        return peli;
    }

    public Pelicula registrarAnio(javax.swing.JTextField txtAnioEstreno) {
        String a = txtAnioEstreno.getText();
        int anio = Integer.parseInt(a);
        peli.setDuracion(anio);
        return peli;
    }
      
    //metodo para setear la calificacion

    /**
     *
     * @param calificacion
     * @return
     */
    public Pelicula seleccionarCalificacion(String calificacion){
        peli.setCalificacion(calificacion);
        return peli;
    }

    public void confirmarReg(javax.swing.JButton btnConfirmar) {
        //todavía no sé cómo hacer la confirmación y la cancelación, lo dejo para lo último
        repertorio1.registrarPelicula(peli);
    }

    public void cancelar(JButton btnCancelar) {
        peli = null;
    }
    
}
