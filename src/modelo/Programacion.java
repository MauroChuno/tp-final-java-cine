package modelo;

import java.time.LocalDate;

public class Programacion {

    private LocalDate fechaFin;
    private LocalDate fechaHoraCreada;
    private LocalDate fechaInicio;
    
    public Programacion(LocalDate fechaFin, LocalDate fechaHoraCreada, LocalDate fechaInicio) {
        this.fechaFin = fechaFin;
        this.fechaHoraCreada = fechaHoraCreada;
        this.fechaInicio = fechaInicio;
    }

    /**
     * @return the fechaFin
     */
    public LocalDate getFechaFin() {
        return fechaFin;
    }

    /**
     * @param fechaFin the fechaFin to set
     */
    public void setFechaFin(LocalDate fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * @return the fechaHoraCreada
     */
    public LocalDate getFechaHoraCreada() {
        return fechaHoraCreada;
    }

    /**
     * @param fechaHoraCreada the fechaHoraCreada to set
     */
    public void setFechaHoraCreada(LocalDate fechaHoraCreada) {
        this.fechaHoraCreada = fechaHoraCreada;
    }

    /**
     * @return the fechaInicio
     */
    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    /**
     * @param fechaInicio the fechaInicio to set
     */
    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public void estaCompleta() {

    }

    public void estaIniciadaFuncion() {

    }

    public void estaVigente() {

    }

    public void mostrarProgramacion() {

    }

}
