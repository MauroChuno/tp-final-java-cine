package modelo;

public class Genero {

    private String nombre;

    public Genero(String nombre) {
        this.nombre = nombre;
    }

    public Genero() {
        
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
