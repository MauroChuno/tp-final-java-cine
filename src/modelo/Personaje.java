package modelo;

public class Personaje {

    private String nombreEnPelicula;
    private Rol rol;

    public Personaje(String nombreEnPelicula, Rol rol) {
        this.nombreEnPelicula = nombreEnPelicula;
    }

    /**
     * @return the nombreEnPelicula
     */
    public String getNombreEnPelicula() {
        return nombreEnPelicula;
    }

    /**
     * @param nombreEnPelicula the nombreEnPelicula to set
     */
    public void setNombreEnPelicula(String nombreEnPelicula) {
        this.nombreEnPelicula = nombreEnPelicula;
    }

    @Override
    public String toString() {
        return "Personaje{" + "nombreEnPelicula=" + nombreEnPelicula + ", rol=" + rol + '}';
    }
    
   

}
