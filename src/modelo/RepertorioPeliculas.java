package modelo;

import Controladora.ControladoraCine;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RepertorioPeliculas {

    List<Pelicula> repertorio = new ArrayList<>();
    private ControladoraCine control1;

    public void registrarPelicula(Pelicula p) {
        if (repertorio.contains(p)) {
            System.out.println("La película ya existe.");
        } else {
            repertorio.add(p);
            Collections.sort(repertorio, new Comparator<Pelicula>() {
                @Override
                public int compare(Pelicula t, Pelicula t1) {
                    return t.getNombre().compareToIgnoreCase(t1.getNombre());
                }
            });
            System.out.println("Película registrada exitosamente.");
        }
    }

    public void mostrarRepertorio() {
        int posicion = 1;
        for (Pelicula pelicula : repertorio) {
            System.out.println(posicion + ".- " + pelicula.getNombre());
            posicion++;
        }
    }

    public void quitarPeliculaPorPosicion(int posicion) {
        this.repertorio.remove(posicion);
    }

    public void quitarPeliculaPorNombre(String name) {
        Collator comparador = Collator.getInstance();
        comparador.setStrength(Collator.PRIMARY);
        for (int i = 0; i < repertorio.size(); i++) {
            Pelicula get = repertorio.get(i);
            if (comparador.compare(get.getNombre(), name) == 0) {
                this.repertorio.remove(i);
            }
        }
    }

    public void setControladoraCine(ControladoraCine control1) {
        this.control1 = control1;
    }
}
