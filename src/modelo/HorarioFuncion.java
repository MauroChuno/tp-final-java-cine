package modelo;

import java.sql.Time;
import java.time.LocalDate;

public class HorarioFuncion {

    private LocalDate diaDeSemana;
    private double duracionInvervalo;
    private double duracionPublicidad;
    private boolean esTrasnoche;
    private Time horaPrimeraFuncion;
    private Time horaUltimaFuncion;

    public HorarioFuncion(LocalDate diaDeSemana, double duracionInvervalo, double duracionPublicidad, boolean esTrasnoche, Time horaPrimeraFuncion, Time horaUltimaFuncion) {
        this.diaDeSemana = diaDeSemana;
        this.duracionInvervalo = duracionInvervalo;
        this.duracionPublicidad = duracionPublicidad;
        this.esTrasnoche = esTrasnoche;
        this.horaPrimeraFuncion = horaPrimeraFuncion;
        this.horaUltimaFuncion = horaUltimaFuncion;
    }

    /**
     * @return the diaDeSemana
     */
    public LocalDate getDiaDeSemana() {
        return diaDeSemana;
    }

    /**
     * @param diaDeSemana the diaDeSemana to set
     */
    public void setDiaDeSemana(LocalDate diaDeSemana) {
        this.diaDeSemana = diaDeSemana;
    }

    /**
     * @return the duracionInvervalo
     */
    public double getDuracionInvervalo() {
        return duracionInvervalo;
    }

    /**
     * @param duracionInvervalo the duracionInvervalo to set
     */
    public void setDuracionInvervalo(double duracionInvervalo) {
        this.duracionInvervalo = duracionInvervalo;
    }

    /**
     * @return the duracionPublicidad
     */
    public double getDuracionPublicidad() {
        return duracionPublicidad;
    }

    /**
     * @param duracionPublicidad the duracionPublicidad to set
     */
    public void setDuracionPublicidad(double duracionPublicidad) {
        this.duracionPublicidad = duracionPublicidad;
    }

    /**
     * @return the esTrasnoche
     */
    public boolean isEsTrasnoche() {
        return esTrasnoche;
    }

    /**
     * @param esTrasnoche the esTrasnoche to set
     */
    public void setEsTrasnoche(boolean esTrasnoche) {
        this.esTrasnoche = esTrasnoche;
    }

    /**
     * @return the horaPrimeraFuncion
     */
    public Time getHoraPrimeraFuncion() {
        return horaPrimeraFuncion;
    }

    /**
     * @param horaPrimeraFuncion the horaPrimeraFuncion to set
     */
    public void setHoraPrimeraFuncion(Time horaPrimeraFuncion) {
        this.horaPrimeraFuncion = horaPrimeraFuncion;
    }

    /**
     * @return the horaUltimaFuncion
     */
    public Time getHoraUltimaFuncion() {
        return horaUltimaFuncion;
    }

    /**
     * @param horaUltimaFuncion the horaUltimaFuncion to set
     */
    public void setHoraUltimaFuncion(Time horaUltimaFuncion) {
        this.horaUltimaFuncion = horaUltimaFuncion;
    }

    public void mostrarHorarioFuncion() {

    }

}
