package modelo;

public class Actor {

    private boolean animado;
    private String apellido;
    private String nombre;

    public Actor(boolean animado, String apellido, String nombre) {
        this.animado = animado;
        this.apellido = apellido;
        this.nombre = nombre;
    }

    /**
     * @return the animado
     */
    public boolean isAnimado() {
        return animado;
    }

    /**
     * @param animado the animado to set
     */
    public void setAnimado(boolean animado) {
        this.animado = animado;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
