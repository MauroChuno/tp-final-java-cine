package modelo;

import java.sql.Time;
import java.time.LocalDate;

public class Funcion {

    private LocalDate diaDeSemana;
    private Double duracion;
    private Time horaInicio;
    private int numero;

    public Funcion(LocalDate diaDeSemana, Double duracion, Time horaInicio, int numero) {
        this.diaDeSemana = diaDeSemana;
        this.duracion = duracion;
        this.horaInicio = horaInicio;
        this.numero = numero;
    }

    /**
     * @return the diaDeSemana
     */
    public LocalDate getDiaDeSemana() {
        return diaDeSemana;
    }

    /**
     * @param diaDeSemana the diaDeSemana to set
     */
    public void setDiaDeSemana(LocalDate diaDeSemana) {
        this.diaDeSemana = diaDeSemana;
    }

    /**
     * @return the duracion
     */
    public Double getDuracion() {
        return duracion;
    }

    /**
     * @param duracion the duracion to set
     */
    public void setDuracion(Double duracion) {
        this.duracion = duracion;
    }

    /**
     * @return the horaInicio
     */
    public Time getHoraInicio() {
        return horaInicio;
    }

    /**
     * @param horaInicio the horaInicio to set
     */
    public void setHoraInicio(Time horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * @return the numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void calcularDisponibilidad() {

    }

    public void capacidadSala() {

    }

    public void estaEnCurso() {

    }

    public void hayLugar() {

    }

    public void mostrarDiaHora() {

    }
}
