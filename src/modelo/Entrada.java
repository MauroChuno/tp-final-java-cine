package modelo;

import java.time.LocalDate;

public class Entrada {

    private LocalDate FechaHoraFuncion;
    private LocalDate FechaHoraVenta;
    private double precioCobrado;
    private int ticketNumero;

    public Entrada(LocalDate FechaHoraFuncion, LocalDate FechaHoraVenta, double precioCobrado, int ticketNumero) {
        this.FechaHoraFuncion = FechaHoraFuncion;
        this.FechaHoraVenta = FechaHoraVenta;
        this.precioCobrado = precioCobrado;
        this.ticketNumero = ticketNumero;
    }

    /*public boolean estaAnulada() {
        boolean estado = false;
        if (0 != this.FechaHoraFuncion && this.precioCobrado != 0){
        } else {
            estado = true;
        }
        return estado;
    }*/

    /**
     * @return the FechaHoraFuncion
     */
    public LocalDate getFechaHoraFuncion() {
        return FechaHoraFuncion;
    }

    /**
     * @param FechaHoraFuncion the FechaHoraFuncion to set
     */
    public void setFechaHoraFuncion(LocalDate FechaHoraFuncion) {
        this.FechaHoraFuncion = FechaHoraFuncion;
    }

    /**
     * @return the FechaHoraVenta
     */
    public LocalDate getFechaHoraVenta() {
        return FechaHoraVenta;
    }

    /**
     * @param FechaHoraVenta the FechaHoraVenta to set
     */
    public void setFechaHoraVenta(LocalDate FechaHoraVenta) {
        this.FechaHoraVenta = FechaHoraVenta;
    }

    /**
     * @return the precioCobrado
     */
    public double getPrecioCobrado() {
        return precioCobrado;
    }

    /**
     * @param precioCobrado the precioCobrado to set
     */
    public void setPrecioCobrado(double precioCobrado) {
        this.precioCobrado = precioCobrado;
    }

    /**
     * @return the ticketNumero
     */
    public int getTicketNumero() {
        return ticketNumero;
    }

    /**
     * @param ticketNumero the ticketNumero to set
     */
    public void setTicketNumero(int ticketNumero) {
        this.ticketNumero = ticketNumero;
    }
    
    public void estaAnulada(){
        
    }

}
