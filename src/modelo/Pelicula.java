package modelo;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;

public class Pelicula {

    private int anioEstreno;
    private boolean disponible;
    private double duracion;
    private LocalDate fechaIngreso;
    private String nombre;
    private String tituloOriginal;
    private HashSet actores = new HashSet();
    private HashSet paisOrigen = new HashSet();
    private HashSet genero = new HashSet();
    private Personaje personaje;
    private String calificacion;
    private boolean isOriginal;
    private boolean isRemake;
    private boolean isRemastered;
    
    public Pelicula (){
        
    }

    public Pelicula(String nombre) {
        this.nombre = nombre;
    }
    
    public Pelicula (String nombre, String tituloOriginal){
        this (nombre);
        this.tituloOriginal = tituloOriginal;
    }
    
    public Pelicula (String nombre, String tituloOriginal, int anioEstreno){
        this (nombre, tituloOriginal);
        this.anioEstreno = anioEstreno;
    }
    
    public Pelicula (String nombre, String tituloOriginal, int anioEstreno, double duracion){
        this (nombre, tituloOriginal, anioEstreno);
        this.duracion = duracion;
    }

    public Pelicula(String nombre, String tituloOriginal, int anioEstreno, double duracion, LocalDate fechaIngreso) {
        this (nombre, tituloOriginal, anioEstreno, duracion);
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * @return the añoEstreno
     */
    public int getAnioEstreno() {
        return anioEstreno;
    }

    /**
     * @param anioEstreno
     */
    public void setAnioEstreno(int anioEstreno) {
        this.anioEstreno = anioEstreno;
    }

    /**
     * @return the disponible
     */
    public boolean isDisponible() {
        return disponible;
    }

    /**
     * @param disponible the disponible to set
     */
    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    /**
     * @return the duracion
     */
    public double getDuracion() {
        return duracion;
    }

    /**
     * @param duracion the duracion to set
     */
    public void setDuracion(double duracion) {
        this.duracion = duracion;
    }

    /**
     * @return the fechaIngreso
     */
    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    /**
     * @param fechaIngreso the fechaIngreso to set
     */
    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the tituloOriginal
     */
    public String getTituloOriginal() {
        return tituloOriginal;
    }

    /**
     * @param tituloOriginal the tituloOriginal to set
     */
    public void setTituloOriginal(String tituloOriginal) {
        this.tituloOriginal = tituloOriginal;
    }

    /**
     * @return the paisOrigen
     */
    public HashSet getPaisOrigen() {
        return paisOrigen;
    }

    /**
     * @param paisOrigen the paisOrigen to set
     * @return 
     */
    public PaisDeOrigen setPaisOrigen(PaisDeOrigen paisOrigen) {
        return paisOrigen;
    }

    /**
     * @return the genero
     */
    public HashSet getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     * @return 
     */
    public Genero setGenero(Genero genero) {
        return genero;
    }

    /**
     * @return the personaje
     */
    public Personaje getPersonaje() {
        return personaje;
    }

    /**
     * @param personaje the personaje to set
     */
    public void setPersonaje(Personaje personaje) {
        this.personaje = personaje;
    }

    public void setActor(Actor a) {
        if (actores.contains(a)) {
            System.out.println("El actor ya existe.");
        } else {
            actores.add(a);
            System.out.println("Actor registrado existosamente.");
        }
    }

    public HashSet getActores() {
        return actores;
    }

    public boolean isIsOriginal() {
        return isOriginal;
    }

    public void setIsOriginal(boolean isOriginal) {
        this.isOriginal = isOriginal;
    }

    public boolean getIsRemake() {
        return isRemake;
    }

    public void setIsRemake(boolean isRemake) {
        this.isRemake = isRemake;
    }

    public boolean isIsRemastered() {
        return isRemastered;
    }

    public void setIsRemastered(boolean isRemastered) {
        this.isRemastered = isRemastered;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.anioEstreno;
        hash = 29 * hash + Objects.hashCode(this.nombre);
        hash = 29 * hash + Objects.hashCode(this.tituloOriginal);
        hash = 29 * hash + Objects.hashCode(this.actores);
        hash = 29 * hash + (this.isRemastered ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pelicula other = (Pelicula) obj;
        if (this.anioEstreno != other.anioEstreno) {
            return false;
        }
        if (this.isRemastered != other.isRemastered) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.tituloOriginal, other.tituloOriginal)) {
            return false;
        }
        if (!Objects.equals(this.actores, other.actores)) {
            return false;
        }
        return true;
    }

    public double calcularDuracionEnFuncion() {
        double resultado = 0;
        return resultado;
    }

    public void estaDisponible() {
        if (this.disponible == true) {
            System.out.println("Disponible");
        } else {
            System.out.println("No Disponible");
        }
    }

    public void estaEnCartelera() {

    }

    public void mostrarFuncionesHabilitadas() {
        /* f.hayLugar tiene que ser true y estaEnCurso tiene que ser falso.*/
    }

    public void setActores(Actor a) {
        if (actores.contains(a)) {
            System.out.println("Actor ya registrado.");
        } else {
            actores.add(a);
            System.out.println("Actor registrado exitosamente.");
        }
    }

    @Override
    public String toString() {
        return "Pelicula{" + "Año de Estreno=" + anioEstreno + ", duracion=" + duracion + ", fechaIngreso=" + fechaIngreso + ", nombre=" + nombre + ", tituloOriginal=" + tituloOriginal + ", actores=" + actores + ", paisOrigen=" + paisOrigen + ", genero=" + genero + ", personaje=" + personaje + '}';
    }

    /**
     * @return the calificacion
     */
    public String getCalificacion() {
        return calificacion;
    }

    /**
     * @param calificacion the calificacion to set
     */
    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

}
