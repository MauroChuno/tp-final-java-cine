package modelo;

import java.time.LocalDate;

public class Cine {

    private String direccion;
    private LocalDate fechaInauguracion;
    private String nombre;
    private double precioEntrada;

    public Cine(String direccion, LocalDate fechaInauguracion, String nombre, double precioEntrada) {
        this.direccion = direccion;
        this.fechaInauguracion = fechaInauguracion;
        this.nombre = nombre;
        this.precioEntrada = precioEntrada;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the fechaInauguracion
     */
    public LocalDate getFechaInauguracion() {
        return fechaInauguracion;
    }

    /**
     * @param fechaInauguracion the fechaInauguracion to set
     */
    public void setFechaInauguracion(LocalDate fechaInauguracion) {
        this.fechaInauguracion = fechaInauguracion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the precioEntrada
     */
    public double getPrecioEntrada() {
        return precioEntrada;
    }

    /**
     * @param precioEntrada the precioEntrada to set
     */
    public void setPrecioEntrada(double precioEntrada) {
        this.precioEntrada = precioEntrada;
    }

    public void mostrarCine() {

    }

    public void mostrarInfoHorariosFuncion() {

    }

}
