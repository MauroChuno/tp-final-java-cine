/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.HashSet;

/**
 *
 * @author Mauro y Gisele
 */
public class PruebaModelo {

    public static void main(String[] args) {
        RepertorioPeliculas r1 = new RepertorioPeliculas();

        r1.registrarPelicula(new Pelicula("avatar"));
        r1.registrarPelicula(new Pelicula("La Sirenita"));
        r1.registrarPelicula(new Pelicula("Los Pitufos"));
        r1.registrarPelicula(new Pelicula("avatar"));
        r1.registrarPelicula(new Pelicula("Cachivache"));

        Pelicula remasterizada = new Pelicula("Spiderman");
        remasterizada.setIsRemastered(true);
        r1.registrarPelicula(remasterizada);

        r1.mostrarRepertorio();
        System.out.println("===================");
        r1.quitarPeliculaPorNombre("Spiderman");
        System.out.println("===================");
        r1.mostrarRepertorio();
        System.out.println("===================");
        r1.quitarPeliculaPorPosicion(1);
        System.out.println("Mostrando repertorio final");
        r1.mostrarRepertorio();

    }
}
